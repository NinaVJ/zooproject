import enums.AnimalSize;
import enums.AnimalType;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AlgorithmTest {

    @Test
    public void testScenario1(){
        //ARRANGE
        //creating correct train object
        Train trainCorrect = new Train();
        Wagon wagon1 = new Wagon();
        wagon1.addAnimal(new Animal(AnimalSize.LARGE, AnimalType.CARNIVORE));
        trainCorrect.addWagon(wagon1);
        Wagon wagon2 = new Wagon();
        wagon2.addAnimal(new Animal(AnimalSize.LARGE, AnimalType.CARNIVORE));
        trainCorrect.addWagon(wagon2);
        Wagon wagon3 = new Wagon();
        wagon3.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE));
        wagon3.addAnimal(new Animal(AnimalSize.LARGE, AnimalType.HERBIVORE));
        trainCorrect.addWagon(wagon3);
        Wagon wagon4 = new Wagon();
        wagon4.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE));
        wagon4.addAnimal(new Animal(AnimalSize.LARGE, AnimalType.HERBIVORE));
        trainCorrect.addWagon(wagon4);
        Wagon wagon5 = new Wagon();
        wagon5.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE));
        wagon5.addAnimal(new Animal(AnimalSize.LARGE, AnimalType.HERBIVORE));
        trainCorrect.addWagon(wagon5);
        Wagon wagon6 = new Wagon();
        wagon6.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE));
        trainCorrect.addWagon(wagon6);
        Wagon wagon7 = new Wagon();
        wagon7.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE));
        trainCorrect.addWagon(wagon7);
        Wagon wagon8 = new Wagon();
        wagon8.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.CARNIVORE));
        wagon8.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        wagon8.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        wagon8.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        trainCorrect.addWagon(wagon8);
        Wagon wagon9 = new Wagon();
        wagon9.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.CARNIVORE));
        wagon9.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        wagon9.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        wagon9.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        trainCorrect.addWagon(wagon9);
        Wagon wagon10 = new Wagon();
        wagon10.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.CARNIVORE));
        wagon10.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        trainCorrect.addWagon(wagon10);
        Wagon wagon11 = new Wagon();
        wagon11.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.CARNIVORE));
        trainCorrect.addWagon(wagon11);
        Wagon wagon12 = new Wagon();
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        trainCorrect.addWagon(wagon12);
        Wagon wagon13 = new Wagon();
        wagon13.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon13.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        trainCorrect.addWagon(wagon13);


        List<Animal> animals = new ArrayList<Animal>();
        animals.add(new Animal(AnimalSize.LARGE, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.LARGE, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.LARGE, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.LARGE, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.LARGE, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));

        //ACT
        //Running algorithm to test with
        Train trainActual = Main.runAlgorithm(animals);

        //ASSERT
        //Compare expected train to train generated by algorithm
        assertTrue(trainCorrect.equals(trainActual));
    }

    @Test
    public void testScenario2(){
        //creating correct train object
        Train trainCorrect = new Train();
        Wagon wagon1 = new Wagon();
        wagon1.addAnimal(new Animal(AnimalSize.LARGE, AnimalType.CARNIVORE));
        trainCorrect.addWagon(wagon1);
        Wagon wagon2 = new Wagon();
        wagon2.addAnimal(new Animal(AnimalSize.LARGE, AnimalType.CARNIVORE));
        trainCorrect.addWagon(wagon2);
        Wagon wagon3 = new Wagon();
        wagon3.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE));
        wagon3.addAnimal(new Animal(AnimalSize.LARGE, AnimalType.HERBIVORE));
        trainCorrect.addWagon(wagon3);
        Wagon wagon4 = new Wagon();
        wagon4.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE));
        wagon4.addAnimal(new Animal(AnimalSize.LARGE, AnimalType.HERBIVORE));
        trainCorrect.addWagon(wagon4);
        Wagon wagon5 = new Wagon();
        wagon5.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE));
        wagon5.addAnimal(new Animal(AnimalSize.LARGE, AnimalType.HERBIVORE));
        trainCorrect.addWagon(wagon5);

        Wagon wagon8 = new Wagon();
        wagon8.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.CARNIVORE));
        wagon8.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        wagon8.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        wagon8.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        trainCorrect.addWagon(wagon8);
        Wagon wagon9 = new Wagon();
        wagon9.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.CARNIVORE));
        wagon9.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        wagon9.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        wagon9.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        trainCorrect.addWagon(wagon9);
        Wagon wagon10 = new Wagon();
        wagon10.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.CARNIVORE));
        wagon10.addAnimal(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        trainCorrect.addWagon(wagon10);
        Wagon wagon11 = new Wagon();
        wagon11.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.CARNIVORE));
        trainCorrect.addWagon(wagon11);
        Wagon wagon12 = new Wagon();
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon12.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        trainCorrect.addWagon(wagon12);
        Wagon wagon13 = new Wagon();
        wagon13.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        wagon13.addAnimal(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        trainCorrect.addWagon(wagon13);


        List<Animal> animals = new ArrayList<Animal>();
        animals.add(new Animal(AnimalSize.LARGE, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.LARGE, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.CARNIVORE));
        animals.add(new Animal(AnimalSize.LARGE, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.LARGE, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.LARGE, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));
        animals.add(new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE));

        //Running algorithm to test with
        Train trainActual = Main.runAlgorithm(animals);
        //Compare expected train to train generated by algorithm
        assertTrue(trainCorrect.equals(trainActual));
    }
}
