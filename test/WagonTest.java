import enums.AnimalSize;
import enums.AnimalType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class WagonTest {
    Animal LargeCarnivore;
    Animal MediumCarnivore;
    Animal SmallCarnivore;
    Animal LargeHerbivore;
    Animal MediumHerbivore;
    Animal SmallHerbivore;

    @Before//before each test in this class this method will run
    public void BeforeTest() {
        //ARRANGE before each test
        LargeCarnivore = new Animal(AnimalSize.LARGE, AnimalType.CARNIVORE);
        MediumCarnivore = new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE);
        SmallCarnivore = new Animal(AnimalSize.SMALL, AnimalType.CARNIVORE);
        LargeHerbivore = new Animal(AnimalSize.LARGE, AnimalType.HERBIVORE);
        MediumHerbivore = new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE);
        SmallHerbivore = new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE);
    }

    @Test
    public void testWagonStartsEmpty(){
        //Test if a wagon starts empty
        Wagon wagon1 = new Wagon();
        assertEquals(0, wagon1.getCurrentWagonWeight());
    }

    @Test
    public void testAddOneAnimal(){

        //ARRANGE
        Wagon wagon1 = new Wagon();

        //ACT
        //Add Large animal and test if animal weight is counted correctly
        wagon1.addAnimal(LargeCarnivore);

        //ASSERT
        assertEquals(5, wagon1.getCurrentWagonWeight());


        //Add Medium animal to an empty wagon and test if animal weight is counted correctly
        Wagon wagon2 = new Wagon();
        wagon2.addAnimal(MediumHerbivore);
        assertEquals(3, wagon2.getCurrentWagonWeight());

        //Add Small Herbivore to an empty wagon and test if animal weight is counted correctly
        Wagon wagon3 = new Wagon();
        wagon3.addAnimal(SmallHerbivore);
        assertEquals(1, wagon3.getCurrentWagonWeight());
    }

    @Test
    public void testAddAnimals(){
        //Add Small Herbivore and Medium Carnivore to an empty wagon and test if animal weight is counted correctly
        Wagon wagon = new Wagon();
        wagon.addAnimal(SmallHerbivore);
        wagon.addAnimal(MediumCarnivore);
        assertEquals(4, wagon.getCurrentWagonWeight());
    }

    @Test
    public void testAddMoreThan10Weight(){
        //Add more animals than weight 10 to an empty wagon and test if it goes over 10.
        // It will go wrong because I have not implemented this yet
        Wagon wagon = new Wagon();
        wagon.addAnimal(LargeHerbivore);
        wagon.addAnimal(LargeHerbivore);
        wagon.addAnimal(MediumCarnivore);
        assertEquals(10, wagon.getCurrentWagonWeight());
    }
}
