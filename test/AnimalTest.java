import enums.AnimalSize;
import enums.AnimalType;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class AnimalTest {
    Animal LargeCarnivore;
    Animal MediumCarnivore;
    Animal SmallCarnivore;
    Animal LargeHerbivore;
    Animal MediumHerbivore;
    Animal SmallHerbivore;

    @Before//before each test in this class this method will run
    public void BeforeTest() {
        LargeCarnivore = new Animal(AnimalSize.LARGE, AnimalType.CARNIVORE);
        MediumCarnivore = new Animal(AnimalSize.MEDIUM, AnimalType.CARNIVORE);
        SmallCarnivore = new Animal(AnimalSize.SMALL, AnimalType.CARNIVORE);
        LargeHerbivore = new Animal(AnimalSize.LARGE, AnimalType.HERBIVORE);
        MediumHerbivore = new Animal(AnimalSize.MEDIUM, AnimalType.HERBIVORE);
        SmallHerbivore = new Animal(AnimalSize.SMALL, AnimalType.HERBIVORE);
    }

    /**
     * Tests whether an Animal returns the correct weight value, based on their AnimalSize
     */
    @Test
    public void WeightTest() {
        //Check if a large animal returns the correct value
        assertEquals(5, LargeCarnivore.getAnimalSize());

        //Check if a medium animal returns the correct value
        assertEquals(3, MediumCarnivore.getAnimalSize());

        //Check if a small animal returns the correct value
        assertEquals(1, SmallCarnivore.getAnimalSize());
    }
}
