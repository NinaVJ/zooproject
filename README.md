# Zoo Application

# INTRODUCTION
*explain why I wanted to create the Zoo application*

## Table of Content
+ Assignment
+ Algorithm 
+ Tests
+ Refactored

## Assignment
There is a circus with a lot of animals. When the circus moves, the animals also have to be moved. The animals have to be put in wagons to transport them. However, the rent of wagons is very expensive. Therefore the amount of wagons added to the train needs to be kept at a minimum. 
A wagon has a maximum weight of 10. There are 2 types of animals: herbivores and carnivores. The animals can be of size Small with a weight of 1, Medium with a weight of 3 and Large with a weight of 5.

In addition there are a few rules:
Herbivores can be put together
Carnivores can never be put together
Carnivores eat all animals that are the same size as themselves or smaller 

Assignment: 
Write an algorithm that efficiently distributes the animals in wagons while using the least amount of wagons to do so


## Algorithm
How I came up with the solution:

First, I reduced the assignments to logical components (see 01). 

![](images/Picture_1.png)

Then, I reduced the information to classes I would need for the solution (02)

![](images/Picture_2.png)

Before I started to code I designed a suitable algorithm. I did so by first thinking of the main logical steps (03).

![](images/Picture_3.png)

Then I wrote pseudocode for each step (04)
For my next step I started to code. Whenever I encountered a problem in my line of thinking I would visualize the issue by drawing or writing about it (see examples).

## Tests

## Refactored

## Attachments



