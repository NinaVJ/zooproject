import java.util.ArrayList;
import java.util.List;


public class Wagon {

    int currentWagonWeight;
    private int maxWagonWeight = 10;//cant change
    List<Animal> animalList;

    public Wagon() {
        this.currentWagonWeight = 0;
        this.animalList = new ArrayList<Animal>();
    }

    public int getCurrentWagonWeight() {
        return currentWagonWeight;
    }

    public void setCurrentWagonWeight(int currentWagonWeight) {
        this.currentWagonWeight = currentWagonWeight;
    }

    public int getMaxWagonWeight() {
        return maxWagonWeight;
    }

    public void setMaxWagonWeight(int maxWagonWeight) {
        this.maxWagonWeight = maxWagonWeight;
    }

    public List<Animal> getAnimalList() {
        return animalList;
    }

    public void setAnimalList(List<Animal> animalList) {
        this.animalList = animalList;
    }

    //add animal to wagon and increase weight according to animal weight

    /**
     * This method adds an Animal object to this Wagon
     * @param animal the Animal object to be added
     */
    public void addAnimal(Animal animal){
        this.animalList.add(animal);
        this.currentWagonWeight += animal.getAnimalSize();
    }

    @Override
    public boolean equals(Object objectToCompare) {
        //check if objectToCompare is equal to this Train
        if (this == objectToCompare) return true;
        if (objectToCompare == null || getClass() != objectToCompare.getClass()) return false;
        Wagon wagonToCompare = (Wagon) objectToCompare;
        if(this.currentWagonWeight != wagonToCompare.currentWagonWeight) return false;
        if(animalList.size() != wagonToCompare.animalList.size()) return false;
        for(int i = 0; i < animalList.size(); i++){
            if(!animalList.get(i).equals(wagonToCompare.animalList.get(i))) return false;
        }
        return true;
    }
}
