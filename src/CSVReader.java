import enums.AnimalSize;
import enums.AnimalType;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSVReader {

    public static ArrayList<Animal> readCSV() {
        //select file
        String path = "/Users/femverkouterenjansen/Documents/Studie/HBO-ICT- Software Engineering/Semester1_Versnelling_RuweVersie/05.Zoo_App/AnimalList1.csv";
        String line = "";
        ArrayList<Animal> animalList = new ArrayList<Animal>();
        List<String[]> listOfAnimals = new ArrayList<>();
        //go through file using a buffer reader
        try {//surrounded with try catch in case the file is not found
            BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
            //go infinitely through the file while the next line is not empty
            while ((line = bufferedReader.readLine()) != null) {
                listOfAnimals.add(line.split(";"));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace(); //if something goes prong the StackTrace is printed
        } catch (IOException e) {
            e.printStackTrace();
        }

        for(String[] array : listOfAnimals){
            animalList.add(new Animal(AnimalSize.valueOf(array[0].toUpperCase()), AnimalType.valueOf(array[1].toUpperCase())));
        }
        return animalList;
    }
}
