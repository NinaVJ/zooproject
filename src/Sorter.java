import enums.AnimalSize;
import enums.AnimalType;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Sorter {
    public static ArrayList<Animal> sortList(ArrayList<Animal> listToBeSorted) {
        ArrayList<Animal> sortedList = new ArrayList<Animal>();

        //TODO: make bubble sort here plz
        int first, second;
        Animal temporary;
        boolean swapped;

        for (first = 0; first < listToBeSorted.size() - 1; first++)
        {
            swapped = false;
            for (second = 0; second < listToBeSorted.size() - first - 1; second++)
            {
                if (listToBeSorted.get(second).getAnimalValue() > listToBeSorted.get(second + 1).getAnimalValue())
                {
                    temporary = listToBeSorted.get(second);
                    listToBeSorted.set(second, listToBeSorted.get(second + 1));
                    listToBeSorted.set(second + 1, temporary);
                    swapped = true;
                }
            }
            if(!swapped) break;
        }

        sortedList = listToBeSorted;
        return sortedList;
    }
}
