import enums.AnimalType;
import enums.AnimalSize;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        ArrayList<Animal> animalList = new ArrayList<Animal>();

        Scanner inputNumberOfAnimals = new Scanner(System.in);
        Scanner sizeScan = new Scanner(System.in);
        Scanner typeScan = new Scanner(System.in);

        System.out.println("Do you want to:");
        System.out.println("1) enter animals manually");
        System.out.println("2) load the file");
        int choice = inputNumberOfAnimals.nextInt();

        if(choice == 1) {

            System.out.println("Enter the number of animals to be shipped in wagons:");
            //numerical user input
            int numberOfAnimals = inputNumberOfAnimals.nextInt();

            //show user input
            System.out.println(numberOfAnimals + " animals need to be shipped in wagons without eating each other!");

            for (int i = 1; i <= numberOfAnimals; i++) {//start at 1 in sysout//todo fix this
                System.out.println("Is animal number " + i + " SMALL, MEDIUM or LARGE?:");
                String size = sizeScan.nextLine().toUpperCase();//convert to uppercase if lowercase

                System.out.println("Is animal number " + i + " a HERBIVORE or CARNIVORE:");
                String type = typeScan.nextLine().toUpperCase();//convert to uppercase if lowercase

                //add animal object and match string input with enum value
                animalList.add(new Animal(AnimalSize.valueOf(size), AnimalType.valueOf(type)));
            }

        }
        else if(choice == 2){
            animalList = CSVReader.readCSV();
        }

        //Sort the list
        animalList = Sorter.sortList(animalList);

        for(Animal animal : animalList){
            System.out.println(animal.animalSize.toString() + animal.animalType.toString());
        }


//        //ask user amount of animals only for user input
//        Scanner inputNumberOfAnimals = new Scanner(System.in);
//        System.out.println("Enter the number of animals to be shipped in wagons:");
//        //numerical user input
//        int numberOfAnimals = inputNumberOfAnimals.nextInt();
//        System.out.println(numberOfAnimals + " animals need to be shipped in wagons without eating each other!");

//        //animalList vullen met user input, of CSV file
//        List<Animal> animalList = new ArrayList<Animal>();
//
        //animalList = AnimalList.sortList(animalList);
        //sort the list created (with user input)
        //todo also sort by Type
//        Collections.sort(animalList, new Comparator<Animal>() {
//            @Override
//            public int compare(Animal animal, Animal t1) {
//                return t1.animalSize.compareTo(animal.animalSize);
//            }
//        });
//        for(int i = 0; i < animalList.size(); i++){
//            System.out.println(animalList.get(i).animalSize + " " + animalList.get(i).animalType);
//        }
        //System.out.println(animalList.toString());

        Train train = runAlgorithm(animalList);
    }

    public static Train runAlgorithm(List<Animal> animals) {

        //create instance of train
        Train train = new Train();

        //counters:
        int largeCarnivores = 0;
        int mediumCarnivores = 0;
        int smallCarnivores = 0;
        int largeHerbivores = 0;
        int mediumHerbivores = 0;
        int smallHerbivores = 0;

        //get amount of animals per type and and weight
        for (Animal animal : animals) {
            int currentAnimalSize = animal.getAnimalSize();
            //convert string to ENUM
            AnimalType currentAnimalType = animal.getAnimalType();

            switch (currentAnimalSize) {
                case 5:
                    if (currentAnimalType == AnimalType.CARNIVORE) largeCarnivores++;
                    else largeHerbivores++;
                    break;
                case 3:
                    if (currentAnimalType == AnimalType.CARNIVORE) mediumCarnivores++;
                    else mediumHerbivores++;
                    break;
                case 1:
                    if (currentAnimalType == AnimalType.CARNIVORE) smallCarnivores++;
                    else smallHerbivores++;
                    break;
                default:
                    System.out.println("you suck at making hardcoded lists...");
            }
        }

        System.out.println("there are " + largeCarnivores + "LC " + largeHerbivores + "LH " + mediumCarnivores + "MC " + mediumHerbivores + "MH " + smallCarnivores + "SC " + smallHerbivores + "SH");

        //Step 1: put all Large Carnivores in separate wagons
        for (Animal animal :
                new ArrayList<Animal>(animals)) {
            if (animal.animalType == AnimalType.CARNIVORE && animal.animalSize == AnimalSize.LARGE) {
                //instantiate a Wagon & add Animal to animalList in wagon (also increases weight)
                Wagon wagon = new Wagon();
                train.addWagon(wagon);
                wagon.addAnimal(animal);
                animals.remove(animal);
                //keep count of animals left to distribute among wagons
                largeCarnivores--;
                System.out.println(largeCarnivores + " large carnivores left.");
            }
        }

        //Step 2: Put all Medium Carnivores in separate wagons and combine with Large Herbivores until on of them runs out
        for (Animal mediumCarnivore :
                new ArrayList<Animal>(animals)) {
            if (mediumCarnivore.animalType == AnimalType.CARNIVORE && mediumCarnivore.animalSize == AnimalSize.MEDIUM) {
                //instantiate a Wagon & add Animal to animalList in wagon (also increases weight)
                Wagon wagon = new Wagon();
                train.addWagon(wagon);
                wagon.addAnimal(mediumCarnivore);
                animals.remove(mediumCarnivore);
                mediumCarnivores--;
                System.out.println(mediumCarnivores + " medium carnivores left.");
                //add Large Herbivores to the same wagon as the Medium Carnivores
                for (Animal largeHerbivore :
                        new ArrayList<Animal>(animals)) {
                    if (largeHerbivore.animalType == AnimalType.HERBIVORE && largeHerbivore.animalSize == AnimalSize.LARGE && 10 - wagon.getCurrentWagonWeight() >= 5) {
                        wagon.addAnimal(largeHerbivore);
                        animals.remove(largeHerbivore);
                        largeHerbivores--;
                        System.out.println(largeHerbivores + " large herbivores left");
                    }
                }
            }
        }

        //Step 3: Put all Small Carnivores in separate wagons and combine with herbivores

        for (Animal smallCarnivore : new ArrayList<Animal>(animals)) {
            if (smallCarnivore.animalType == AnimalType.CARNIVORE && smallCarnivore.animalSize == AnimalSize.SMALL) {
                Wagon wagon = new Wagon();
                train.addWagon(wagon);
                wagon.addAnimal(smallCarnivore);
                animals.remove(smallCarnivore);
                smallCarnivores--;

                if (mediumHerbivores >= 3) {
                    for (Animal mediumHerbivore : new ArrayList<Animal>(animals)) {
                        if (mediumHerbivore.animalType == AnimalType.HERBIVORE && mediumHerbivore.animalSize == AnimalSize.MEDIUM && 10 - wagon.getCurrentWagonWeight() >= 3) {
                            wagon.addAnimal(mediumHerbivore);
                            animals.remove(mediumHerbivore);
                            mediumHerbivores--;
                        }
                    }
                } else {
                    for (Animal animal : new ArrayList<Animal>(animals)) {
                        if (animal.animalType == AnimalType.HERBIVORE && animal.animalSize == AnimalSize.MEDIUM && wagon.getCurrentWagonWeight() == 1) {
                            wagon.addAnimal(animal);
                            animals.remove(animal);
                            mediumHerbivores--;
                        }
                        if (animal.animalType == AnimalType.HERBIVORE && animal.animalSize == AnimalSize.LARGE && wagon.getCurrentWagonWeight() <= 5) {
                            wagon.addAnimal(animal);
                            animals.remove(animal);
                            largeHerbivores--;
                        }
                    }
                }
            }
        }

        //Step 4: put the remaining herbivores in wagons (only herbivores left in the list from now on...)
        //put large herbivores in wagons in pairs, possibly one left if uneven number
        while (largeHerbivores > 1) {
            Wagon wagon = new Wagon();
            train.addWagon(wagon);
            for (Animal animal : new ArrayList<Animal>(animals)) {
                if (animal.animalSize == AnimalSize.LARGE && wagon.getCurrentWagonWeight() <= 5) {
                    wagon.addAnimal(animal);
                    animals.remove(animal);
                    largeHerbivores--;
                }//possibly one Large herbivore left to put in wagon
            }
        }
        //put all medium herbivores in wagons three at a time if 3 or more medium carnivores are left
        while (mediumHerbivores >= 3) {
            Wagon wagon = new Wagon();
            train.addWagon(wagon);
            for (Animal animal : new ArrayList<Animal>(animals)) {
                if (animal.animalSize == AnimalSize.MEDIUM && 10 - wagon.getCurrentWagonWeight() >= 3) {
                    wagon.addAnimal(animal);
                    animals.remove(animal);
                    mediumHerbivores--;
                }//possible 0 tot 2 medium herbivores left to put in wagon
            }

            //todo: add small herbivore
        }

        //if there is still 1 Large Herbivore put in separate wagon
        if (largeHerbivores == 1) {
            Wagon wagon = new Wagon();
            train.addWagon(wagon);
            for (Animal animal : new ArrayList<Animal>(animals)) {
                if (animal.animalSize == AnimalSize.LARGE) {
                    wagon.addAnimal(animal);
                    animals.remove(animal);
                    largeHerbivores--;
                }
            }
            //if medium herbivores left, put in the same wagon
            if (mediumHerbivores > 0) {
                for (Animal animal : new ArrayList<Animal>(animals)) {
                    if (animal.animalSize == AnimalSize.MEDIUM && 10 - wagon.getCurrentWagonWeight() >= 3) {
                        wagon.addAnimal(animal);
                        animals.remove(animal);
                        mediumHerbivores--;
                    }
                }
            }
            //if small herbivores left, put in the same wagon until wagon is full or no small herbivores left
            for (Animal animal : new ArrayList<Animal>(animals)) {
                if (animal.animalSize == AnimalSize.SMALL && 10 - wagon.getCurrentWagonWeight() >= 1) {
                    wagon.addAnimal(animal);
                    animals.remove(animal);
                    smallHerbivores--;
                }
            }
        }//no large herbivores left

        //if any medium or small herbivores are left they will be distributed among wagons
        while (smallHerbivores + mediumHerbivores > 0) {
            Wagon wagon = new Wagon();
            train.addWagon(wagon);

            if (mediumHerbivores > 0) {
                for (Animal animal : new ArrayList<Animal>(animals)) {
                    if (animal.animalSize == AnimalSize.MEDIUM) {
                        wagon.addAnimal(animal);
                        animals.remove(animal);
                        mediumHerbivores--;
                    }
                }
            }
            //put all remaining animals (only small herbivores) in wagons
            while (wagon.getCurrentWagonWeight() < 10) {
                if (animals.size() > 0) {
                    wagon.addAnimal(animals.get(0));
                    animals.remove(0);
                    smallHerbivores--;
                } else break;
            }
        }


        System.out.println(" ");
        System.out.println("PRINTING TRAIN...");
        //Print entire train to console
        for (Wagon wagon : train.getWagons()) {
            System.out.println("Wagon: " + (train.getWagons().indexOf(wagon) + 1) + " - current weight: " + wagon.getCurrentWagonWeight());
            for (Animal animal : wagon.getAnimalList()) {
                System.out.println("Animal: " + (wagon.getAnimalList().indexOf(animal) + 1)
                        + " - Size: " + animal.animalSize.toString() + " / Type: " + animal.animalType.toString());
            }
            System.out.println(" ");
        }

        System.out.println(" ");
        System.out.println("PRINTING REMAINING ANIMALS...");
        for (Animal animal : animals) {
            System.out.println(animal.animalSize.toString() + " " + animal.animalType.toString());
        }

        return train;
    }

}
