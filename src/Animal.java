import enums.AnimalSize;
import enums.AnimalType;

public class Animal {

    AnimalSize animalSize;//default so I can use in other classes
    AnimalType animalType;

    //constructor to create an animal

    public Animal(AnimalSize animalSize, AnimalType animalType) {
        this.animalSize = animalSize;
        this.animalType = animalType;
    }

    //getters & setters for animal so that I can create objects
    public int getAnimalSize() {
        switch (this.animalSize){
            case SMALL:
                return 1;
            case MEDIUM:
                return 3;
            case LARGE:
                return 5;
            default:
                return 0;
        }
    }

    public void setAnimalSize(AnimalSize animalSize) {
        this.animalSize = animalSize;
    }

    public AnimalType getAnimalType() {
        return animalType;
    }

    public void setAnimalType(AnimalType animalType) {
        this.animalType = animalType;
    }

    public int getAnimalValue(){
        if(this.animalSize == AnimalSize.LARGE && this.animalType == AnimalType.CARNIVORE)
        {
            return 1;
        } else if(this.animalSize == AnimalSize.MEDIUM && this.animalType == AnimalType.CARNIVORE){
            return 2;
        } else if(this.animalSize == AnimalSize.SMALL&& this.animalType == AnimalType.CARNIVORE) {
            return 3;
        } else if(this.animalSize == AnimalSize.LARGE && this.animalType == AnimalType.HERBIVORE){
            return 4;
        } else if(this.animalSize == AnimalSize.MEDIUM && this.animalType == AnimalType.HERBIVORE){
            return 5;
        } else if(this.animalSize == AnimalSize.SMALL && this.animalType == AnimalType.HERBIVORE) {
            return 6;
        }
        else return 0;
    }

    @Override
    public boolean equals(Object objectToCompare) {
        //check if objectToCompare is equal to this Train
        if (this == objectToCompare) return true;
        if (objectToCompare == null || getClass() != objectToCompare.getClass()) return false;
        Animal animalToCompare = (Animal) objectToCompare;
        return animalSize == animalToCompare.animalSize && animalType == animalToCompare.animalType;
    }
}
