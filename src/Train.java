import java.util.ArrayList;
import java.util.List;

public class Train {

    List<Wagon> wagons;

    //constructor
    public Train() {
        this.wagons = new ArrayList<Wagon>();
    }

    //getter and setter
    public List<Wagon> getWagons() {
        return wagons;
    }

    public void setWagons(List<Wagon> wagons) {
        this.wagons = wagons;
    }

    public int getTrainSize(){
        return wagons.size();
    }

    public void addWagon(Wagon wagon){
        this.wagons.add(wagon);
    }

    @Override
    public boolean equals(Object objectToCompare) {
        //check if objectToCompare is equal to this Train
        if (this == objectToCompare) return true;
        if (objectToCompare == null || getClass() != objectToCompare.getClass()) return false;
        Train trainToCompare = (Train) objectToCompare;
        if(wagons.size() != trainToCompare.wagons.size()) return false;
        for(int i = 0; i < wagons.size(); i++){
            if(!wagons.get(i).equals(trainToCompare.wagons.get(i))) return false;
        }
        return true;
    }
}
